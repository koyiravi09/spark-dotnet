﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spark.Utilities.Common
{
    class AppConstants
    {
        public const string SpiWeb = "SpiWeb";
        public const string SpiAnnotate = "SpiAnnotate";
        public const string SpiPdfExtract = "SpiPdfExtract";
        public const string Reports = "Reports";
        public const string Scripts = "Scripts";
        public const string Processed = "Processed";
        public const string Ingested = "Ingested";
        public const string interim = "Interim";
        public const string Final = "Final";

    }
}
