﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using Newtonsoft.Json.Linq;
using Spark.Utilities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Spark.Utilities
{
    public class AWSServices
    {
        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = Amazon.RegionEndpoint.APSouth1;
        private static IAmazonS3 s3Client;
        public static string awsAccessKeyId = "AKIAXMZKJKCMOJB2OXGX";
        public static string awsSecretAccessKey = "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";
   

        public async Task CreateBucket(string bucketName)
        {
            try
            {
                s3Client = new AmazonS3Client(awsAccessKeyId, awsSecretAccessKey, bucketRegion);
                var jobject = new JObject();

                if (!(await AmazonS3Util.DoesS3BucketExistV2Async(s3Client, bucketName)))
                {
                    var putBucketRequest = new PutBucketRequest
                    {
                        BucketName = bucketName,
                        BucketRegion = S3Region.APS3,
                        CannedACL = S3CannedACL.PublicReadWrite

                    };

                    PutBucketResponse putBucketResponse = await s3Client.PutBucketAsync(putBucketRequest);
                }
            }

            catch (AmazonS3Exception ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task CreateAWSFolder(string bucketName, string projectName)
        {
            string folder = bucketName + "/" + projectName;
            if(!String.IsNullOrEmpty(folder))
            {
                await CreateFolder(bucketName, folder);
            }
        }

        public async Task CreateAWSFolder(string bucketName, string projectName, Int32? serviceId)
        {
            string folder1 = null;
            string folder2 = null;
            string folder3 = null;
            if (serviceId == 1)
            {
                folder1 = AppConstants.SpiWeb + "/" + AppConstants.Reports + "/";
                folder2 = AppConstants.SpiWeb + "/" + AppConstants.Scripts + "/";
                folder3 = AppConstants.SpiWeb + "/" + AppConstants.Processed + "/";
            }
            else if (serviceId == 2)
            {
                folder1 = AppConstants.SpiAnnotate + "/" + AppConstants.Ingested + "/";
                folder2 = AppConstants.SpiAnnotate + "/" + AppConstants.interim + "/";
                folder3 = AppConstants.SpiAnnotate + "/" + AppConstants.Final + "/";
            }
            else if (serviceId == 3)
            {
                folder1 = AppConstants.SpiPdfExtract + "/" + AppConstants.Ingested + "/";
                folder2 = AppConstants.SpiPdfExtract + "/" + AppConstants.interim + "/";
                folder3 = AppConstants.SpiPdfExtract + "/" + AppConstants.Final + "/";
            }
            if (!String.IsNullOrEmpty(folder1))
            {
                folder1 = projectName + "/" + folder1;
                await CreateFolder(bucketName, folder1);
            }
            if (!String.IsNullOrEmpty(folder2))
            {
                folder2 = projectName + "/" + folder2;
                await CreateFolder(bucketName, folder2);
            }
            if (!String.IsNullOrEmpty(folder3))
            {
                folder3 = projectName + "/" + folder3;
                await CreateFolder(bucketName, folder3);
            }
        }

        public async Task CreateFolder(string bucketName, string folderPath)
        {
            try
            {
                s3Client = new AmazonS3Client(awsAccessKeyId, awsSecretAccessKey, bucketRegion);

                var findFolderRequest = new ListObjectsV2Request();
                findFolderRequest.BucketName = bucketName;
                findFolderRequest.Prefix = folderPath;

                ListObjectsV2Response findFolderResponse = await s3Client.ListObjectsV2Async(findFolderRequest);

                if (findFolderResponse.S3Objects.Any())
                {
                    return;
                }

                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketName,
                    Key = folderPath,
                };
                PutObjectResponse response = await s3Client.PutObjectAsync(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteAWSFolder(string bucketName, Int32? serviceId)
        {
            string folder1 = null;
            string folder2 = null;
            string folder3 = null;
            if (serviceId == 1)
            {
                folder1 = AppConstants.SpiWeb + "/" + AppConstants.Reports + "/";
                folder2 = AppConstants.SpiWeb + "/" + AppConstants.Scripts + "/";
                folder3 = AppConstants.SpiWeb + "/" + AppConstants.Processed + "/";
            }
            else if (serviceId == 2)
            {
                folder1 = AppConstants.SpiAnnotate + "/" + AppConstants.Ingested + "/";
                folder2 = AppConstants.SpiAnnotate + "/" + AppConstants.interim + "/";
                folder3 = AppConstants.SpiAnnotate + "/" + AppConstants.Final + "/";
            }
            else if (serviceId == 3)
            {
                folder1 = AppConstants.SpiPdfExtract + "/" + AppConstants.Ingested + "/";
                folder2 = AppConstants.SpiPdfExtract + "/" + AppConstants.interim + "/";
                folder3 = AppConstants.SpiPdfExtract + "/" + AppConstants.Final + "/";
            }
            if (!String.IsNullOrEmpty(folder1))
                await DeleteFolder(bucketName, folder1);
            if (!String.IsNullOrEmpty(folder2))
                await DeleteFolder(bucketName, folder2);
            if (!String.IsNullOrEmpty(folder3))
                await DeleteFolder(bucketName, folder3);
        }

        public async Task DeleteFolder(string bucketName, string folderPath)
        {
            try
            {
                s3Client = new AmazonS3Client(awsAccessKeyId, awsSecretAccessKey, bucketRegion);

                var deleteFolderRequest = new DeleteObjectRequest
                {
                    BucketName = bucketName,
                    Key = folderPath
                };
                DeleteObjectResponse folderDeleteResponse = await s3Client.DeleteObjectAsync(deleteFolderRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task UploadFilesToAWS(dynamic file,string awsBucketName)
        {

            
                try
                {
                    var s3Client = new AmazonS3Client(awsAccessKeyId, awsSecretAccessKey, Amazon.RegionEndpoint.APSouth1);
                    var bucketName = awsBucketName;
                    //var keyName = AWS_defaultFolder + "/" + AWS_subFolder.Trim() + "/" + file.FileName;
                    var keyName = AppConstants.SpiAnnotate + "/" + AppConstants.Ingested + "/" + file.FileName;


                    var request = new Amazon.S3.Model.PutObjectRequest
                    {
                        BucketName = bucketName,
                        Key = keyName,
                        InputStream = file.OpenReadStream(),
                        ContentType = file.ContentType,
                        CannedACL = S3CannedACL.PublicRead
                       
                    };
                    await s3Client.PutObjectAsync(request);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            

            
        }

    }
}


