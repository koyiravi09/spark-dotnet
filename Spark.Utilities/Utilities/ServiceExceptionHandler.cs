﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;


namespace Spark.Utilities
{
    public class ServiceExceptionHandler : IExceptionFilter
    {
        HttpStatusCode statusCode;
        string errorMessage = null;
       

        public void OnException(ExceptionContext context)
        {
            HttpResponse response = context.HttpContext.Response;
            string result = null;


            if (context.Exception as WebException != null &&
               ((HttpWebResponse)(context.Exception as WebException).Response) != null) {
               statusCode = ((HttpWebResponse)(context.Exception as WebException).Response).StatusCode;
                 
                 errorMessage = context.Exception.Message;
                string stackTrace = context.Exception.StackTrace;

               
                response.StatusCode = (int)statusCode;
                response.ContentType = "application/json";
                 result = JsonConvert.SerializeObject(
                    new
                    {
                        errorMessage = errorMessage,
                        errorCode = statusCode
                    });
                response.ContentLength = result.Length;
                response.WriteAsync(result);
            }
            else
            {
                Type exType = context.Exception.GetType();
                string exName = exType.Name;

                JObject exCode = getExceptionJson(exName);
                if (exCode != null)
                {
                    Int32 statusCode = (Int32)exCode?.GetValue("code");
                    string message = (string)exCode?.GetValue("message");

                    response.StatusCode = statusCode;
                    response.ContentType = "application/json";
                    result = JsonConvert.SerializeObject(
                       new
                       {
                           errorMessage = message,
                           errorCode = statusCode
                       });
                    response.ContentLength = result.Length;
                    response.WriteAsync(result);
                }
                else
                {
                    result = JsonConvert.SerializeObject(
                      new
                      {
                          errorMessage = "Bad Request",
                          errorCode = 400
                      });
                    response.ContentLength = result.Length;
                    response.WriteAsync(result);
                }
                
            }

        }


        public static JObject getExceptionJson(string exName)
        {
            Console.WriteLine("exName" + exName);

            JObject exCode = null;
           
            var buildDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var filePath = buildDir + @"\Utilities\ExceptionDetails.json";
           
            var myJsonString = File.ReadAllText(filePath);


            if(!String.IsNullOrEmpty(myJsonString))
            {
                var jObject = JObject.Parse(myJsonString);
                if(jObject!= null)
                {
                    exCode = jObject?.SelectToken(exName)?.Value<JObject>();
                }
            }

            return exCode;
        }
    }
}
