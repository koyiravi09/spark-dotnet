﻿using Microsoft.Extensions.Configuration;
using NLog;
using NLog.AWS.Logger;
using NLog.Config;
using NLog.Targets;
using System;
using System.IO;
using System.Security.Principal;
using System.Threading;

namespace Spark.Utilities
{
    public class Logger
    {

        /// <summary>
        /// 
        /// </summary>
        public Logger()
        {
            try
            {
                IConfigurationBuilder builder = new ConfigurationBuilder();
                LoggingConfiguration config = new LoggingConfiguration();
               
                builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), Constants.appSettingJson));
              

                var root = builder.Build();
                string targetType = root.GetSection(Constants.sparkLog).GetSection(Constants.targetType).Value;
                var logLevelInput = root.GetSection(Constants.sparkLog).GetSection(Constants.minLevel).Value;
                var aws = root.GetSection(Constants.sparkLog).GetSection(Constants.targets).GetSection(Constants.aws);
                LogLevel LogLevel = getLogLevel(logLevelInput);

                if (targetType == Constants.aws)
                {
                    var logGroup = aws.GetSection(Constants.logGroup).Value;
                    var region = aws.GetSection(Constants.region).Value;
                    string accessKey = aws.GetSection(Constants.accessKey).Value;
                    string secretKey = aws.GetSection(Constants.secretKey).Value;

                    var awsTarget = new AWSTarget()
                    {
                        LogGroup = logGroup,
                        Region = region,
                        Credentials = new Amazon.Runtime.BasicAWSCredentials(accessKey, secretKey)
                    };
                    config.AddTarget(targetType, awsTarget);
                    config.LoggingRules.Add(new LoggingRule("*", LogLevel, awsTarget));
                    LogManager.Configuration = config;
                }
                else if (targetType == Constants.elastic)
                {
                    var url = root.GetSection(Constants.sparkLog).GetSection(Constants.targets).GetSection(Constants.elastic).GetSection(Constants.url).Value;

                }
                else
                {
                    var filePath = root.GetSection(Constants.sparkLog).GetSection(Constants.targets).GetSection(Constants.file).GetSection(Constants.filePath).Value;

                    var fileTarget = new FileTarget()
                    {
                        FileName = filePath
                    };
                    config.AddTarget(targetType, fileTarget);
                    config.LoggingRules.Add(new LoggingRule("*", LogLevel, fileTarget));
                    LogManager.Configuration = config;
                }

              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LogLevel getLogLevel(string logLevelInput)
        {
            LogLevel logLevel = null;
            if (logLevelInput == Constants.debug)
            {
                logLevel = LogLevel.Debug;
            }
            else if (logLevelInput == Constants.trace)
            {
                logLevel = LogLevel.Trace;
            }
            else if (logLevelInput == Constants.error)
            {
                logLevel = LogLevel.Error;
            }
            else if (logLevelInput == Constants.fatal)
            {
                logLevel = LogLevel.Fatal;
            }
            else if (logLevelInput == Constants.warn)
            {
                logLevel = LogLevel.Warn;
            }
            else if (logLevelInput == Constants.info)
            {
                logLevel = LogLevel.Info;
            }
            return logLevel;

        }

        #region log
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="categoryid"></param>
        /// <param name="priorityid"></param>
        public static void log(Exception ex, Int32 categoryid, Int32 priorityid)
        {
             var logger = NLog.LogManager.GetCurrentClassLogger(); 
          
            switch (categoryid)
            {
                case (Int32)Category.Fatal:
                    logger.Fatal($"{getDetails(ex,priorityid, Category.Fatal)}"); break;
                case (Int32)Category.Error:
                    logger.Error($"{getDetails(ex, priorityid, Category.Error)}"); break; 
                case (Int32)Category.Warn:
                    logger.Warn($"{getDetails(ex, priorityid, Category.Warn)}"); break;
                default:
                    logger.Trace($"{getDetails(ex, priorityid, Category.Trace)}"); break;
            }
        }

        public static void log(String message, Int32 categoryid, Int32 priorityid)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();

            switch (categoryid)
            {
                case (Int32)Category.Info:
                    logger.Info($"{getDetails(message, priorityid, Category.Info)}"); break;
                case (Int32)Category.Debug:
                    logger.Debug($"{getDetails(message, priorityid, Category.Debug)}"); break;
                default:
                    logger.Trace($"{getDetails(message, priorityid, Category.Trace)}"); break;
            }
        }


        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="priorityId"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        private static string getDetails(Exception ex, Int32 priorityId, Category category)
        {
            var exType = ex.GetType().Name;
            var exMsg = ex.Message;
            var exStackTrace = ex.StackTrace;
            var threadName = Thread.CurrentThread.Name;
            var userName = WindowsIdentity.GetCurrent().Name;
            var priority = Constants.getEnumValue<Priority>(priorityId);

            string result = "Type: " + exType + " Priority: " + priority + " Category: " + category + " Message: " + exMsg + " StackTrace: " + exStackTrace + " ThreadName: " + threadName + " UserName: " + userName;
            return result;
        }

        private static string getDetails(String message, Int32 priorityId, Category category)
        {
            var threadName = Thread.CurrentThread.Name;
            var userName = WindowsIdentity.GetCurrent().Name;
            var priority = Constants.getEnumValue<Priority>(priorityId);

            string result = " Priority: " + priority + " Category: " + category + " Message: " + message + " ThreadName: " + threadName + " UserName: " + userName;
            return result;
        }


    }
}
