﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spark.Utilities
{
    public static class Constants 
    {
       
        public const string appSettingJson = "appsettings.json";
        public const string debug = "Debug";
        public const string fatal = "Fatal";
        public const string info = "Info";
        public const string error = "Error";
        public const string warn = "Warn";
        public const string trace = "Trace";
        public const string sparkLog = "Spark-Log";
        public const string targetType = "targetType";
        public const string minLevel = "minLevel";
        public const string targets = "targets";
        public const string aws = "aws";
        public const string file = "file";
        public const string elastic = "elastic";
        public const string logGroup = "logGroup";
        public const string region = "region";
        public const string accessKey = "accessKey";
        public const string secretKey = "secretKey";
        public const string url = "url";
        public const string filePath = "filePath";
        public static string getEnumValue<T>(this Int32 value)
        {
            var name = Enum.GetName(typeof(T), value);
            return name;
        }
    }
    public enum Priority
    {
        Lowest ,
        Low ,
        Normal ,
        High ,
        Highest 

    }

   public enum Category
    {
        Fatal,
        Trace,
        Debug,
        Error,
        Warn,
        Info
    }
}
