﻿using Microsoft.AspNetCore.Mvc;
using Spark.OnBoardingServices;
using Spark.OnBoardingServices.Controllers;
using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Xunit;

namespace Spark.OnBoardingService.Test
{
    public class UserTest
    {
        UserController userController ;
        public UserTest()
        {
            userController = new UserController();
        }

        [Fact]
        public async void InsertUser()
        {
            RegisterUser user = new RegisterUser()
            {
                  userId = 1,
                  firstName = "Jaya",
                  lastName = "Kumar",
                  displayName = "Jayanth",
                  emailId = "jayanth@121999@gmail.com",
                  isLDAP = true,
                  password = "jayanth24",
                  department = "Computerscience",
           
                  processType = "insert"
             };

            var jsonElement = GetJsonElement<RegisterUser>(user);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void UpdateUser()
        {
            RegisterUser user = new RegisterUser()
            {
                userId = 1,
                firstName = "Jayanth",
                lastName = "pandi",
                displayName = "Jayakumar",
                emailId = "kumar@121999@gmail.com",
                isLDAP = false,
                password = "kumar24",
                department = "Hardware",
               
                processType = "update"
            };

            var jsonElement = GetJsonElement<RegisterUser>(user);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void DeleteUser()
        {
            RegisterUser user = new RegisterUser()
            {
                userId = 1,
                processType = "delete"
            };

            var jsonElement = GetJsonElement<RegisterUser>(user);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetUserById()
        {
            User user = new User()
            {
                userId = 1,
                processType = "get"
            };

            var jsonElement = GetJsonElement<User>(user);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientByLimtt()
        {
            User user = new User()
            {
                startIndex = 1,
                limit = 20,
                processType = "get"
            };

            var jsonElement = GetJsonElement<User>(user);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void Get()
        {
            User user = new User()
            {
                processType = "get"
            };
            var jsonElement = GetJsonElement<User>(user);            
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void InsertClientUser()
        {
            ClientUser clientUser = new ClientUser()
            { 
                clientId = 1,
                userId = 2,
                firstName = "kumar",
                clientDescription = "service man",
                processType = "insertClientUser",
            };

            var jsonElement = GetJsonElement<ClientUser>(clientUser);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void UpdateClientUser()
        {
            ClientUser clientUser = new ClientUser()
            {
              clientUserMappingId = 1, 
              clientId =2,
              userId = 1, 
              firstName = "Dinesh",
              clientDescription = "man",  
              processType="updateClientuser"
            };

            var jsonElement = GetJsonElement<ClientUser>(clientUser);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void DeleteClientUser()
        {
            ClientUser clientUser = new ClientUser()
            {               
                clientId = 2,
                userId = 1,
                processType = "deleteClientUser"
            };

            var jsonElement = GetJsonElement<ClientUser>(clientUser);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientUserByClientId()
        {
            ClientUser clientUser = new ClientUser()
            {
                clientId = 2,
                processType= "getClientUserByClientId"
            };
            var jsonElement = GetJsonElement<ClientUser>(clientUser);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);

            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientUserByUserId()
        {
            ClientUser clientUser = new ClientUser()
            {
                userId = 2,
                processType = "getClientUserByUserId"
            };
            var jsonElement = GetJsonElement<ClientUser>(clientUser);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientUser()
        {
            ClientUser clientUser = new ClientUser()
            {
                userId = 1,
                clientId = 2,
                processType = "getClientUser"
            };
            var jsonElement = GetJsonElement<ClientUser>(clientUser);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void InsertUserRole()
        {
            UserRole userRole = new UserRole()
            {
                  roleId = 1,
                  userId =2,
                  roleName = "Developer",
                  userName = "Kumar",
                  roleDiscription = "Developing projects",
                  userDiscription = "provide services",
                  processType = "insertUserRole"
            };

            var jsonElement = GetJsonElement<UserRole>(userRole);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void UpdateUserRole()
        {
            UserRole userRole = new UserRole()
            {
                userRoleMappingId = 1,
                roleId = 1,
                userId = 2,
                roleName = "Developer",
                userName = "Kumar",
                roleDiscription = "Developing projects",
                userDiscription = "provide services",
                processType = "updateUserRole"
            };

            var jsonElement = GetJsonElement<UserRole>(userRole);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void DeleteUserRole()
        {
            UserRole userRole = new UserRole()
            {
                roleId = 1,
                userId = 2,
                processType = "deleteUserRole"
            };

            var jsonElement = GetJsonElement<UserRole>(userRole);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetUserRoleByUserId()
        {
            UserRole userRole = new UserRole()
            {
                userId = 1,
                processType = "getUserRoleByUserId"
            };

            var jsonElement = GetJsonElement<UserRole>(userRole);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetUserRoleByRoleId()
        {
            UserRole userRole = new UserRole()
            {
                roleId = 2,
                processType = "getUserRoleByRoleId"
            };

            var jsonElement = GetJsonElement<UserRole>(userRole);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetUserRole()
        {
            UserRole userRole = new UserRole()
            {
                userId = 1,
                roleId = 2,
               processType = "getUserRole"
            };
            var jsonElement = GetJsonElement<UserRole>(userRole);
            // Act
            var OkResult = await userController.RegisterUser(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }
        private JsonElement GetJsonElement<T>(T obj)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

            var jsonDoc = JsonDocument.Parse(json);
            var jsonElement = jsonDoc.RootElement.Clone();

            return jsonElement;
        }

    }
}
