﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Common
{
    public static class CommonUtil
    {
        public static ResponseModel ResponseObject(Int32? id, Int32? flag)
        {
            try
            {
                ResponseModel response = new ResponseModel();
                string responseMessage = "";

                if (flag == 1)
                {
                    responseMessage = "Inserted successfully";
                }
                else if (flag == 2)
                {
                    responseMessage = "Updated Successfully";
                }
                else if (flag == 3)
                {
                    responseMessage = "Deleted successsfully";
                }

                response.id = id;
                response.message = responseMessage;
                return response;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
