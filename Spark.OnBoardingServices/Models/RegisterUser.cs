﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class RegisterUser : Pagination
    {
       
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string displayName { get; set; }
        public string emailId { get; set; }
        public string department { get; set; }
        public Int32? logInUserId { get; set; }
        public string password { get; set; }
        public Int32? userId { get; set; }
        public bool? isLDAP { get; set; } 
    }
}
