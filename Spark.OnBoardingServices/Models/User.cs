﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices
{
    public class User 
    {
        public string processType;
        public int startIndex;
        public int limit;

        public Int32? userId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public List<string> roleName { get; set; }
        public string displayName { get; set; }
        public string emailId { get; set; }     
        public string department { get; set; }
        public bool isActive { get; set; }
       
    }
}
