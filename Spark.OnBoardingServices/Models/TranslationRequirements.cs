﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class TranslationRequirements
    {
        public string fromLanguage { get; set; }
        public string toLanguage { get; set; }
    }
}
