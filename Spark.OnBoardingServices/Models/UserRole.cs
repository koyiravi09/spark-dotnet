﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class UserRole : Pagination
    {
        public Int32? userRoleMappingId { get; set; }
        public Int32? logInUserId { get; set; }
        public Int32? roleId { get; set; }
        public Int32 userId { get; set; }
        public string roleIds { get; set; }
        public string roleName { get; set; }
        public string userName { get; set; }
        public string roleDiscription { get; set; }
        public string userDiscription { get; set; }
    }
}
