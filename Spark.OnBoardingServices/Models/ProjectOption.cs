﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class ProjectOption : Pagination
    {
        public Int32? projectOptionId { get; set; }
        public Int32? projectId { get; set; }
        public ComplianceRequirements complianceRequirement { get; set; }
        public List<ComplianceRequirements> complianceRequirements { get; set; }
        public StorageArchivalRequirements storageArchivalRequirement { get; set; }
        public TranslationRequirements translationRequirement { get; set; }
        public List<TranslationRequirements> translationRequirements { get; set; }
        public int userId { get; set; }


    }
}
