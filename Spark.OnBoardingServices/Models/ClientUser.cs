﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class ClientUser : Pagination
    {
        public Int32? clientUserMappingId  { get; set; }
        public Int32? clientId { get; set; }
        public Int32? userId { get; set; }
        public Int32? logInUserId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string clientDescription { get; set; }
    }
}
