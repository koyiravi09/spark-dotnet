﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class Service : Pagination
    {
        public Int32? serviceId { get; set; }
        public string serviceName { get; set; }
        public string serviceDescription { get; set; }
        public Int32? userId { get; set; }
        public Int32 isActive { get; set; }
    }
}
