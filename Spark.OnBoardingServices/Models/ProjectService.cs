﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class ProjectService : Pagination
    {
        public Int32? projectServiceId { get; set; }
        public Int32? clientProjectId { get; set; }
        public Int32 serviceId { get; set; }
        public Int32 userId { get; set; }
        public string serviceName { get; set; }
        public string serviceDiscription { get; set; }
    }
}
