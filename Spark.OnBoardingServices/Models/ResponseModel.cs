﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class ResponseModel
    {
        public  Int32? id { get; set; }
        public string message { get; set; }
    }
}
