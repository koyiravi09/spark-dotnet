﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.DAO.Repository;
using Spark.OnBoardingServices.Models;
using Spark.OnBoardingServices.Transactions;
using Spark.Utilities;

namespace Spark.OnBoardingServices.Controllers
{
    [Route("api/psonboardingservices")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUser userData = new UserData();
        private Object userObject;

        [HttpPost("registeruser")]
        public async Task<ActionResult<dynamic>> RegisterUser([FromBody] JsonElement user)
        {
            dynamic finalResponse = "Not Valid Information";
            Logger.log("Inside the Register User", (Int32)Category.Info, (Int32)Priority.High);
            try
            {
                //Console.WriteLine(" Inside the RegisterUser");
                var userObject = getObject(user);

                if (userObject is RegisterUser)
                {
                    RegisterUser userInfo = (RegisterUser)userObject;
                    if (userInfo.processType == AppConstants.Insert)
                    {
                        finalResponse = await userData.InsertUser(userInfo);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (userInfo.processType == AppConstants.Update)
                    {
                        finalResponse = await userData.UpdateUser(userInfo);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (userInfo.processType == AppConstants.Delete)
                    {
                        UnitOfWork unitofwork = new UnitOfWork();
                        finalResponse = await unitofwork.userDelete(userInfo);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (userInfo.processType == AppConstants.Get)
                    {
                        if (userInfo.userId.HasValue)
                        {
                            finalResponse = await userData.GetUser(userInfo.userId);
                            if(finalResponse !=null)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else if (userInfo.startIndex.HasValue)
                        {
                            finalResponse = await userData.GetUser(userInfo.startIndex, userInfo.limit);
                            if (finalResponse != null && finalResponse.Count > 0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else
                        {
                            finalResponse = await userData.GetUser();
                            if (finalResponse != null && finalResponse.Count > 0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                    }
                }

                else if (userObject is ClientUser)
                {
                    var clientUser = (ClientUser)userObject;

                    if (clientUser.processType == AppConstants.InsertClientUser)
                    {
                        finalResponse = await userData.InsertClientUser(clientUser);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (clientUser.processType == AppConstants.UpdateClientUser)
                    {
                        finalResponse = await userData.UpdateClientUser(clientUser);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (clientUser.processType == AppConstants.DeleteClientUser)
                    {
                        finalResponse = await userData.DeleteClientUser(clientUser, null, null);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (clientUser.processType == AppConstants.GetClientUserByClientId)
                    {
                        finalResponse = await userData.GetClientUserByClientId(clientUser.clientId);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if (clientUser.processType == AppConstants.GetClientUserByUserId)
                    {
                        finalResponse = await userData.GetClientUserByUserId(clientUser.userId);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if (clientUser.processType == AppConstants.GetClientUser)
                    {
                        finalResponse = await userData.GetClientUser(clientUser);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                }

                else if (userObject is UserRole)
                {
                    var userRole = (UserRole)userObject;

                    if (userRole.processType == AppConstants.InsertUserRole)
                    {
                        finalResponse = await userData.InsertUserRole(userRole);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (userRole.processType == AppConstants.UpdateUserRole)
                    {
                        finalResponse = await userData.UpdateUserRole(userRole);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (userRole.processType == AppConstants.DeleteUserRole)
                    {
                        finalResponse = await userData.DeleteUserRole(userRole,null,null);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (userRole.processType == AppConstants.GetUserRoleByUserId)
                    {
                        finalResponse = await userData.GetUserRoleByUserId(userRole.userId);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if(userRole.processType == AppConstants.GetUserRoleByRoleIds)
                    {
                        finalResponse = await userData.GetUserRoleByRoleIds(userRole.roleIds);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if (userRole.processType == AppConstants.GetUserRoleByRoleId)
                    {
                        finalResponse = await userData.GetUserRoleByRoleId(userRole.roleId);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if (userRole.processType == AppConstants.GetUserRole)
                    {

                        finalResponse = await userData.GetUserRole(userRole);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                }
                return StatusCode(AppConstants.NoContent, finalResponse);
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return StatusCode(AppConstants.InternalServerError, AppConstants.ErrorMessage);
            }
        }
        private Object getObject(JsonElement userInfo)
        {
            try
            {
                String userString = userInfo.GetRawText();
                JObject obj = JObject.Parse(userString);
                var processType = obj.GetValue("processType").ToString();

                if (processType == AppConstants.Insert || processType == AppConstants.Update
                    || processType == AppConstants.Delete || processType == AppConstants.Get)
                {
                    userObject = JsonConvert.DeserializeObject<RegisterUser>(userString);
                }
                else if (processType == AppConstants.InsertClientUser || processType == AppConstants.UpdateClientUser
                    || processType == AppConstants.DeleteClientUser || processType == AppConstants.GetClientUserByClientId
                    || processType == AppConstants.GetClientUserByUserId || processType == AppConstants.GetClientUser)
                {
                    userObject = JsonConvert.DeserializeObject<ClientUser>(userString);
                   
                }
                else if (processType == AppConstants.InsertUserRole || processType == AppConstants.UpdateUserRole
                    || processType == AppConstants.DeleteUserRole || processType == AppConstants.GetUserRole || processType == AppConstants.GetUserRoleByRoleIds
                    || processType == AppConstants.GetUserRoleByRoleId || processType == AppConstants.GetUserRoleByUserId)
                {
                    userObject = JsonConvert.DeserializeObject<UserRole>(userString);
                }
                return userObject;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
    }
}

