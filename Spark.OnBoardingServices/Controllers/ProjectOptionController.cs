﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.DAO.Repository;
using Spark.OnBoardingServices.Models;
using Spark.Utilities;

namespace Spark.OnBoardingServices.Controllers
{
    [Route("api/psonboardingservices")]
    [ApiController]
    public class ProjectOptionController : ControllerBase
    {
        private IProjectOption projectOptionData = new ProjectOptionData();
        private Object projectOptionObject;

        [HttpPost("registerprojectoption")]
        public async Task<ActionResult<dynamic>> RegisterProjectOption([FromBody] JsonElement projectOption)
        {
            dynamic finalResponse = null;
           
            try
            {
                var projectOptionObject = getObject(projectOption);
                ProjectOption projectOptionInfo = (ProjectOption)projectOptionObject;

                if (projectOptionInfo.processType == AppConstants.InsertComplianceRequirement)
                {

                    finalResponse = await projectOptionData.InsertComplianceRequirements(projectOptionInfo);
                    return StatusCode(AppConstants.Created, finalResponse);
                }

               else if (projectOptionInfo.processType == AppConstants.InsertTranslationRequirement)
                {

                    finalResponse = await projectOptionData.InsertTranslationRequirements(projectOptionInfo);
                    return StatusCode(AppConstants.Created, finalResponse);
                }

               else if (projectOptionInfo.processType == AppConstants.InsertStorageArchivalRequirement)
                {

                    finalResponse = await projectOptionData.InsertStorageArchivalRequirements(projectOptionInfo);
                    return StatusCode(AppConstants.Created, finalResponse);
                }

                else if (projectOptionInfo.processType == AppConstants.UpdateStorageArchivalRequirement)
                {

                    finalResponse = await projectOptionData.UpdateStorageArchivalRequirements(projectOptionInfo);
                    return StatusCode(AppConstants.Success, finalResponse);
                }
                else if (projectOptionInfo.processType == AppConstants.UpdateComplianceRequirement)
                {

                    finalResponse = await projectOptionData.UpdateComplianceRequirements(projectOptionInfo);
                    return StatusCode(AppConstants.Success, finalResponse);
                }

                else if (projectOptionInfo.processType == AppConstants.UpdateTranslationRequirement)
                {

                    finalResponse = await projectOptionData.UpdateTranslationRequirements(projectOptionInfo);
                    return StatusCode(AppConstants.Success, finalResponse);
                }
                else if (projectOptionInfo.processType == AppConstants.GetProjectOptions)
                {

                    finalResponse = await projectOptionData.GetProjectOption(projectOptionInfo);
                    return StatusCode(AppConstants.Success, finalResponse);
                }
               

            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return StatusCode(AppConstants.InternalServerError, AppConstants.ErrorMessage);
            }
            return StatusCode(AppConstants.NoContent, finalResponse);
        }
        private Object getObject(JsonElement projectOptionInfo)
        {
            try
            {
                String projectOptionString = projectOptionInfo.GetRawText();
                JObject obj = JObject.Parse(projectOptionString);
                var processType = obj.GetValue("processType").ToString();
                projectOptionObject = JsonConvert.DeserializeObject<ProjectOption>(projectOptionString);
                return projectOptionObject;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
