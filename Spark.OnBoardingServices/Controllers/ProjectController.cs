﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.DAO.Repository;
using Spark.OnBoardingServices.Models;
using Spark.OnBoardingServices.Transactions;
using Spark.Utilities;

namespace Spark.OnBoardingServices.Controllers
{
    [Route("api/psonboardingservices")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private IProject projectData = new ProjectData();
        private IClient clientData = new ClientData();
        private AWSServices awsServices = new AWSServices();
        private Object projectObject;

        [HttpPost("registerproject")]
        public async Task<ActionResult<dynamic>> RegisterProject([FromBody] JsonElement project)
        {
            dynamic finalResponse = null;
            try
            {
                var projectObject = GetObject(project);

                if (projectObject is Project)
                {
                    Project projectInfo = (Project)projectObject;
                    
                    if (projectInfo.processType == AppConstants.Insert)
                    {    
                        finalResponse = await projectData.InsertProject(projectInfo);
                       // var clientInfo = await clientData.GetClient(finalResponse.clientId);
                       // await awsServices.CreateAWSFolder(clientInfo.bucketName, projectInfo.projectName);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (projectInfo.processType == AppConstants.Update)
                    {
                        finalResponse = await projectData.UpdateProject(projectInfo);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (projectInfo.processType == AppConstants.ProjectStatus)
                    {
                        finalResponse = await projectData.UpdateProjectStatus(projectInfo);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (projectInfo.processType == AppConstants.Delete)
                    {
                        UnitOfWork unitOfWork = new UnitOfWork();
                        finalResponse = await unitOfWork.projectDelete(projectInfo);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (projectInfo.processType == AppConstants.Get)
                    {
                        if (projectInfo.clientProjectId.HasValue)
                        {
                            finalResponse = await projectData.GetProject(projectInfo.clientProjectId);
                            if (finalResponse != null)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else if (projectInfo.startIndex.HasValue)
                        {
                            finalResponse = await projectData.GetProject(projectInfo.startIndex, projectInfo.limit);
                            if (finalResponse != null && finalResponse.Count > 0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else
                        {
                            finalResponse = await projectData.GetProject();
                            if (finalResponse != null && finalResponse.Count > 0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                    }
                }
                else if(projectObject is ProjectService)
                {
                    var projectService = (ProjectService)projectObject;

                    if (projectService.processType == AppConstants.InsertProjectService)
                    {
                        finalResponse = await projectData.InsertProjectService(projectService);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (projectService.processType == AppConstants.UpdateProjectService)
                    { 
                        finalResponse = await projectData.UpdateProjectService(projectService);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (projectService.processType == AppConstants.DeleteProjectService)
                    {
                        finalResponse = await projectData.DeleteProjectService(projectService, null, null);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (projectService.processType == AppConstants.GetProjectServiceByProjectId)
                    {
                        finalResponse = await projectData.GetProjectServiceByProjectId(projectService.clientProjectId);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return StatusCode(AppConstants.InternalServerError, AppConstants.ErrorMessage);
            }
            return StatusCode(AppConstants.NoContent, finalResponse);
        }
        
        private Object GetObject(JsonElement projectInfo)
        {
            try
            {
                String projectString = projectInfo.GetRawText();
                JObject obj = JObject.Parse(projectString);
                var processType = obj.GetValue("processType").ToString();

                if (processType == AppConstants.Insert || processType == AppConstants.Update || processType == AppConstants.ProjectStatus
                    || processType == AppConstants.Delete || processType == AppConstants.Get)
                {
                    projectObject = JsonConvert.DeserializeObject<Project>(projectString);
                }
                else if (processType == AppConstants.InsertProjectService || processType == AppConstants.UpdateProjectService
                    || processType == AppConstants.DeleteProjectService || processType == AppConstants.GetProjectServiceByProjectId)
                {
                    projectObject = JsonConvert.DeserializeObject<ProjectService>(projectString);
                }
                return projectObject;
            }
            catch (Exception ex)
            {
               throw ex;
            }
        }

    }
}
