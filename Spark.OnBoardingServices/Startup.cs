using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Microsoft.OpenApi.Models;
using Spark.Utilities;

namespace Spark.OnBoardingServices
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(name: "v1", new OpenApiInfo { Title = "OnBoarding Services Api", Version = "v1" });
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });
            IdentityModelEventSource.ShowPII = true;
            services.AddAuthentication("Bearer")
              .AddJwtBearer(options =>
              {
                  options.Audience = "7l4h94jdkdds6er19a1jmj1am1";
                  options.Authority = "https://ap-south-1.console.aws.amazon.com/ap-south-1_DghaoxRzD";
              });


            services.AddMvc(config =>
            {
                config.Filters.Add(typeof(ServiceExceptionHandler));
            });

            services.Add(new ServiceDescriptor(typeof(Logger), new Logger()));
            //services.AddAuthentication("Bearer")
            //    .AddJwtBearer(options =>
            //    {
            //        options.Audience = "7l4h94jdkdds6er19a1jmj1am1";
            //        options.Authority = "https://ap-south-1.console.aws.amazon.com/cognito/users/?region=ap-south-1#/pool/ap-south-1_DghaoxRzD/details?_k=i41lmd";
            //    });

        }
           

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
  
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(url: "./v1/swagger.json", name: "OnBoarding Services Api");
            });

            app.UseCors(builder => {
                builder.AllowAnyOrigin();
                builder.AllowAnyMethod();
                builder.AllowAnyHeader();
            });

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
